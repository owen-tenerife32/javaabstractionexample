package lec.java.ex;

import lec.java.objects.GradingSystem;
import lec.java.objects.GradingType;
import lec.java.objects.ListTypeGradingItems;
import lec.java.objects.ListValue;
import lec.java.objects.RangeTypeGradingItems;
import lec.java.objects.RangeValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
    	System.out.println("WELCOME TO MY PROGRAM!");
    	
        Scanner scn = new Scanner(System.in);
        GradingSystem gradingSystem = new GradingSystem();
        GradingType gradingType = null;

        System.out.println("ENTER VALUE [1 => List, 2 => Range] :");
        int choice = scn.nextInt();
        switch (choice) {
            case 1:
                gradingType = new ListTypeGradingItems();
                List<ListValue> grades = new ArrayList<>();
                grades.add(new ListValue(90, "A"));
                grades.add(new ListValue(80, "B"));
                grades.add(new ListValue(70, "C"));
                gradingType.setGrades(grades);
                break;
            case 2:
                gradingType = new RangeTypeGradingItems();
                List<RangeValue> rangeGrades = new ArrayList<>();
                rangeGrades.add(new RangeValue(100, 90, "EXCELLENT"));
                rangeGrades.add(new RangeValue(89, 80, "Very Good"));
                rangeGrades.add(new RangeValue(79, 70, "Almost There! :)"));
                gradingType.setGrades(rangeGrades);
                break;
            default:
        }

        if(gradingType != null) {
            System.out.println(gradingType.getType());
            gradingType.displayGrades();

            System.out.print("ENTER YOUR GRADE PLEASE: ");
            int grade = scn.nextInt();
            System.out.println(gradingType.getEquivalentGrade(grade));
        }
    }
}
