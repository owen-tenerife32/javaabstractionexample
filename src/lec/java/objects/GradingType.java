package lec.java.objects;

import java.util.List;

public abstract class GradingType {
    Type type;
    //Required by JPA
    public GradingType() {
    }
    public Type getType() {
        return type;
    }

    public abstract void setGrades(List obj);
    public abstract void displayGrades();
    public abstract String getEquivalentGrade(int grade);
}
