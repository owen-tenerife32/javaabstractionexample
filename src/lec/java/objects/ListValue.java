package lec.java.objects;

public class ListValue {

    private int grade;
    private String equivalent;

    //Required by JPA
    public ListValue(int grade, String equivalent) {
        this.grade = grade;
        this.equivalent = equivalent;
    }

    public int getGrade() {
        return grade;
    }

    public String getEquivalent() {
        return equivalent;
    }
}
