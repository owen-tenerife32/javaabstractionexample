package lec.java.objects;

import java.util.ArrayList;
import java.util.List;

public class ListTypeGradingItems extends GradingType {

    List<ListValue> grades = new ArrayList<>();

    //Required by JPA
    public ListTypeGradingItems() {
        super();
        super.type = Type.LIST;
    }

    @Override
    public void setGrades(List obj) {
        this.grades = obj;
    }

    @Override
    public void displayGrades() {
        for(ListValue value : grades) {
            System.out.println(value.getGrade());
        }
    }

    @Override
    public String getEquivalentGrade(int grade) {
        for(ListValue value : grades) {
            if(value.getGrade() == grade) {
                return value.getEquivalent();
            }
        }
        return "";
    }

    public List<ListValue> getGrades() {
        return grades;
    }

}
