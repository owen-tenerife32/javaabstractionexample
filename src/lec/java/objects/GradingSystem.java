package lec.java.objects;

public class GradingSystem {

    //[90 => "A", 80 => "B", 70 => "C"] List Type Grading System
    //[100 - 90 => "EXCELLENT",
    // 89 - 80 => "Very Good",
    // 79 - 70 => "Almost There :)"] Range Type Grading System
    GradingType gradingType;

    //Required by JPA
    public GradingSystem() {
    }
}
