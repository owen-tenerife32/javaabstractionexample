package lec.java.objects;

public class RangeValue {

    private int highest;
    private int lowest;
    private String remark;

    //Required by JPA
    public RangeValue(int highest, int lowest, String remark) {
        this.highest = highest;
        this.lowest = lowest;
        this.remark = remark;
    }

    public int getHighest() {
        return highest;
    }

    public int getLowest() {
        return lowest;
    }

    public String getRemark() {
        return remark;
    }

    public boolean isInRange(int grade) {
      return  grade <= highest  && grade >= lowest;
    }
}
