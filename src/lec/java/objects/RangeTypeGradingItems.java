package lec.java.objects;

import java.util.ArrayList;
import java.util.List;

public class RangeTypeGradingItems extends GradingType{

    List<RangeValue> grades = new ArrayList<>();

    //Required by JPA
    public RangeTypeGradingItems() {
        super();
        super.type = Type.RANGE;
    }

    @Override
    public void setGrades(List obj) {
        this.grades = obj;
    }

    @Override
    public void displayGrades() {
        for (RangeValue item: grades) {
            System.out.print("HIGHEST GRADE: " + item.getHighest() + " ");
            System.out.print("LOWEST GRADE: " + item.getLowest());
            System.out.println();
        }
    }

    @Override
    public String getEquivalentGrade(int grade) {
        for (RangeValue value : grades) {
            if(value.isInRange(grade)) {
                return value.getRemark();
            }
        }
        return "";
    }
}
